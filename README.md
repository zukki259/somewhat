# Somewhat  
git cloneに失敗しまくる時に使うレポジトリ. 自由にcloneしてね.  
以下のサイトを参考にしてね  
  
[GithubでSSH通信した時に「Permission denied (publickey).」が発生した時の対処方法。](http://sbkro.hatenablog.jp/entry/2013/04/23/231312)  
[GitLab上のGit操作で「Permission denied」が出た時の対策](https://qiita.com/mcddx330/items/8d36108553030071f403)  
[github や gitlab で clone,pull が Permission denied になる問題と解決](https://golang.hateblo.jp/entry/2018/10/26/073911)  
  
[cygwinでssh接続](https://qiita.com/artk/items/eaf4542746decb2f1110)  
[cygwinでssh-agentをカンタンに使う](https://gist.github.com/Metro-SatoMakoto/c5837345ead9cdc9c6ff)
  
私の場合は(Mac OS使用)  
*  公開鍵をGitLabに登録した
*  ssh gitlabは成功する
*  しかしgit cloneでPermission Deniedになる  

という症状で, ~/.ssh/known-hostのgitlab.comの行を消した上でssh-addで秘密鍵を登録したら行けるようになりました.  
  
その後, WindowsのCygwinでもssh-agentを使用することで同様の手順を踏んでできました.  
Cygwinの場合,  
`
$ eval 'ssh-agent -s' `  
`
$ ssh-add .ssh/key_gitlab
`  
みたいな手順を踏む(文中のクオ―テーションはバッククオートに置き換えてね)とssh-agentが立ち上がるので, 無事git cloneできるようになった.  
これならいけると思ってwindowsのcmd.exeでもやってみたけど, 見事に失敗. まだ原因究明中.  
他にありがちなのは,  
*  ~/.ssh/configに書いておくUser名は"git"
*  鍵を作る時に`ssh-keygen -C "hoge@foobar.com"`と言う風にコメントにログイン用メールアドレスを入れる(のが作法らしい?)  
*  公開鍵のコピペ時は余計な操作をしない(以前1文字消したせいで入れなくなった事例を見た)

辺りかなあと. 備忘録がてら置いておきます.  